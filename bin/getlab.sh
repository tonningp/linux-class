#!/usr/bin/env bash 
#
#export host_base_url="https://boss.vvc.edu/moodle/cis190/content"
export host_base_url="https://cis.vvc.edu/cis190-files"
export host_upload_url='https://cis.vvc.edu/cis190-upload'
export START="$(date +%s)"
export week=''
export full_name=''
export student_id=''
export HTTP_DOWNLOADER='curl'
export current_dir=$(pwd)

function reminder() {
    cat <<EOS

*************************************************************
Remember to press Ctrl+d to complete this portion of the lab.
*************************************************************

EOS
}
export -f reminder

function final_cleanup() {
    cd $current_dir

    if [[ ! -d completed ]];then
        mkdir completed
    fi
    DURATION=$(( $(date +%s) - ${START} ))
    echo "${DURATION}" > labs/duration.dat
    if [[ ${full_name_f}x == x  ]];then
        full_name_f=$USER
    fi
    if [[ ${student_id}x == x  ]];then
        student_id='none'
    fi
    tar_file="${current_dir}/completed/${full_name_f}-${student_id}-week-${week}-$(date +%s).tgz"
    echo "Saving file $tar_file"
    tar zcf $tar_file labs typescripts
    if [[ $(hostname) == workbench ]];then
        host_upload_url='http://127.0.0.1:8888/upload'
        curl -X POST $host_upload_url -F "files=@${tar_file}" -F'token=a9564ebc3289b7a14551baf8ad5ec60a' > /dev/null 2>&1
    else
         cat<<EOS_LAB
Please follow the instructions in the video to upload the lab to the moodle 
server, the instructions are on the Course Resources section on the
course web page: https://boss.vvc.edu/moodle/course/view.php?id=154#course-resources
EOS_LAB
        #curl -X POST $host_upload_url -F "files=@${tar_file}" -F'token=a9564ebc3289b7a14551baf8ad5ec60a' > /dev/null 2>&1
    fi
    rm -rf labs typescripts
    if [[ -f /tmp/current_instructions.${USER} ]];then
        rm /tmp/current_instructions.${USER}
    fi
    exit 0
}

trap "final_cleanup" SIGINT


function display_requirement_instructions() {
    echo "It looks like you are running this on ${1}"
    echo "Install the $HTTP_DOWNLOADER program by running the following command: "
    printf "\n%s\n\n" "$2"
    # further instructions
    if [[ ${3}x != x ]];then
        echo "$3"
    fi
    echo
    echo "Run this program again after you have installed the required package."
    echo
}

function check_has_requirements() {
    if ! command -v $HTTP_DOWNLOADER >/dev/null 2>&1;then
        echo
        echo "This system does not have $HTTP_DOWNLOADER"
        echo
        if [[ "$OSTYPE" == "linux-gnu"* ]]; then
            display_requirement_instructions "debian/ubuntu" "sudo apt-get install $HTTP_DOWNLOADER"
        elif [[ "$OSTYPE" == "darwin"* ]]; then
            export instructions=$(cat <<EOS1
If you do not have brew installed, install it by going to https://brew.sh/ 
and following the instructions on that web site.
EOS1
)
            display_requirement_instructions "MacOS" "brew install $HTTP_DOWNLOADER" "$instructions"
        elif [[ "$OSTYPE" == "cygwin" ]]; then
            display_requirement_instructions "cygwin" "setup-x86_64.exe any time you want to update or install a Cygwin package for 64-bit windows. Look for the $HTTP_DOWNLOADER package."
        # POSIX compatibility layer and Linux environment emulation for Windows
        elif [[ "$OSTYPE" == "msys" ]]; then
            display_requirement_instructions "msys" "pacman $HTTP_DOWNLOADER"
        # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
        elif [[ "$OSTYPE" == "win32" ]]; then
            echo "You are running this on Windows, get a linux system"
        # I'm not sure this can happen.
        elif [[ "$OSTYPE" == "freebsd"* ]]; then
        # ...
            echo "You are running this on freebsd, please install and use a linux system"
        else
            echo "This is an unknown system, please install and use a linux system"
        fi
        exit 1
    fi
}


function check_has_week() {
    if [[ ${1}x == x ]]; then
        echo "usage: $0 week"
        exit 1
    fi
    week=${1}
}



function get_student_info() {
    while [[ ${full_name}x == x ]];do
        read -p "Enter your first and last name: " full_name
    done
    while [[ ${student_id}x == x ]];do
        read -p "Enter your student id: " student_id
    done
}


function prompt() {
    read -p "$1" v
    eval "$2=$v"
}

function answer() {
    cat > answer-$1.txt
}

export -f answer

export current_instructions="instructions"
function save_prompt_text() {
    current_instructions="$1"
    echo "${current_instructions}" > /tmp/current_instructions.${USER}
}
export -f save_prompt_text

function print_instructions() {
    if [[ -f /tmp/current_instructions.${USER} ]];then
        echo '********************************************************************************'
        echo
        cat /tmp/current_instructions.${USER}
        echo
        echo '********************************************************************************'
    else
        echo "instruction file not found."
    fi
}
export -f print_instructions
export attempt=1
function display_question_prompt() {
    echo "********************************************************************************"
    echo "*                                                                              *" 
    echo "*          Follow the instructions below and perform the given task            *"
    echo "*                 type Ctrl+d when you have completed the task                 *" 
    echo "*                                                                              *" 
    echo "********************************************************************************"
    echo
    echo "$1"
    echo
    save_prompt_text "$1"
}
export -f display_question_prompt



export question_prompt=''
function process_exercise {
    exercise=$1
    output="$($HTTP_DOWNLOADER -fsSL ${host_base_url}/${exercise} 2>/dev/null)"
    if [[ $? != 0 ]];then
        echo "Remote exercise does not exist, contact the course instructor"
        exit 1
    fi
    bash -c "$output"
}

function main() {

check_has_requirements    
check_has_week $1

CONTENT=content
get_student_info
export full_name_f=$(echo $full_name | tr [A-Z] [a-z] |tr ' ' '_' )
week_index="week-${week}-exercises.sh"
exercise_index="${host_base_url}/${week_index}"

while true
do
    if [ ! -d "${current_dir}/labs" ]; then
        mkdir ${current_dir}/labs
    else
        rm -rf ${current_dir}/labs/*
    fi
    if [ ! -d "${current_dir}/typescripts" ]; then
        mkdir ${current_dir}/typescripts
    else
        rm -rf ${current_dir}/typescripts/*
    fi
    if [ -d "${current_dir}/completed" ]; then
        rm -rf ${current_dir}/completed
    fi

    IFS_OLD=$IFS
    IFS=$'\n'
    bash_run="exec bash --rcfile <($HTTP_DOWNLOADER -fsSL ${host_base_url}/startup) -i"
    cd "${current_dir}/labs"
    exercises=$($HTTP_DOWNLOADER -fsSL ${exercise_index} 2> /dev/null)
    if [[ $? == 0 ]];then
        export exercise_number=1
        for exercise in $(curl -fsSL ${exercise_index})
        do
            attempt_file="${current_dir}/typescripts/${week}-${full_name_f}-${student_id}-exercise-${exercise_number}.out"
            process_exercise "$exercise"
            script -a -q $attempt_file bash -c $bash_run
            export exercise_number=$(( $exercise_number + 1 ))
        done
    else
        echo "Week parameter was not correct, please enter the correct week and try again"
        echo 
        echo "For example, if this is week 2, then enter"
        echo
        echo "bin/getlab.sh 02"
        echo
        exit 1
    fi
    IFS=$IFS_OLD
prompt_text=$(cat <<EOS_prompt
Attempt # ${attempt} completed.

If you want to attempt the exercises again press y (then Enter).

To end this lab and upload the attempt press q (then Enter) 

> 
EOS_prompt
)
    next=''
    prompt "${prompt_text}" next
    echo $next

    if [[ $next == 'q' ]]
    then
        kill -SIGINT $$
    fi
    attempt=$((attempt+1))
done
}

# start the main function
#
main ${1}
