#!/usr/bin/env bash

export host_upload_url='https://cis.vvc.edu/cis190-upload'
function upload_file() {
    if [[ $(hostname) == workbench ]];then
        host_upload_url='http://127.0.0.1:8888/upload'
    fi
    echo "uploading $1"
    #curl --insecure -X POST $host_upload_url -F "files=@${1}" -F'token=a9564ebc3289b7a14551baf8ad5ec60a' > /dev/null 2>&1
    curl --insecure -X POST $host_upload_url -F "files=@${1}" -F'token=a9564ebc3289b7a14551baf8ad5ec60a' 
    echo $1
}

#if [[ ! -f /usr/bin/dialog ]];then
#    sudo apt update && sudo apt -y install dialog
#fi

#if [[ -f /usr/bin/dialog ]];then
#        dialog --title "Select File" --fselect ./completed/*.tgz 10 80 2>/tmp/filename.txt
#        export filename=$(cat /tmp/filename.txt)
#        rm /tmp/filename.txt
#fi
#dialog --title "Upload $filename?" --backtitle "Upload $filename?" \
#   --yesno "Upload the file $filename to server?"  10 80
cd $HOME/cis190-labs/completed
echo "The following files are in the completed directory: "
echo
ls -1 -r

read -p'Enter the file that you want to upload: ' filename
if [[ ${?} == 0 ]];then
        upload_file $filename
fi
