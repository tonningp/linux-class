# 06 VIM Deep Dive

## Objectives

After reading this chapter you should be able to:

* Use vim to create and edit a file
* View vim online help
* Explain the difference between Command and Input modes
* Explain the purpose of the Work buffer
* List the commands that open a line above the cursor, append text to the end of a line, move the cursor to the first line of the file, and move the cursor to the middle line of the screen
* Describe Last Line mode and list some commands that use this mode
* Describe how to set and move the cursor to a marker
* List the commands that move the cursor backward and forward by characters and words
* Describe how to read a file into the Work buffer
* Explain how to search backward and forward for text and how to repeat that search

## Summary

This summary of vim includes all the commands covered in this chapter, plus a few more. Table 6-12 lists some of the ways you can call vim from the command line.

Table 6-12   Calling vim
| Command                   | Result                                                       |
|---------------------------|--------------------------------------------------------------|
| vim filename              | Edits filename starting at line 1                            |
| vim +n filename           | Edits filename starting at line n                            |
| vim + filename            | Edits filename starting at the last line                     |
| vim +/pattern filename    | Edits filename starting at the first line containing pattern |
| vim -r filename           | Recovers filename after a system crash                       |
| vim -R filename           | Edits filename readonly (same as opening the file with view) |

You must be in Command mode to use commands that move the cursor by Units of Measure (Table 6-13). You can use these Units of Measure with Change, Delete, and Yank commands. Each of these commands can be preceded by a Repeat Factor.

Table 6-13 Moving the cursor by Units of Measure
| Command                          | Moves the cursor                                          |
|----------------------------------|-----------------------------------------------------------|
| SPACE, I (ell), or RIGHT ARROW   | Space to the right                                        |
| h or LEFT ARROW                  | Space to the left                                         |
| W                                | Word to the right                                         |
| W                                | Blank-delimited word to the right                         |
| b                                | Word to the left                                          |
| B                                | Blank-delimited word to the left                          |
| $                                | End of line                                               |
| e                                | End of word to the right                                  |
| E                                | End of blank-delimited word to the right                  |
| 0 (zero)                         | Beginning of line (cannot be used with a Repeat Factor)   |
| RETURN                           | Beginning of next line                                    |
| j or DOWN ARROW                  | Down one line                                             |
| -                                | Beginning of previous line                                |
| k or UP ARROW                    | Up one line                                               |
| )                                | End of sentence                                           |
| (                                | Beginning of sentence                                     |
| }                                | End of paragraph                                          |
| {                                | Beginning of paragraph                                    |
| %                                | Move to matching brace of same type at same nesting level |

Table 6-14 shows the commands that enable you to view different parts of the Work buffer.

Table 6-14 Viewing the Work buffer
| Command                           | Moves the cursor                                          |
|-----------------------------------|-----------------------------------------------------------|
| CONTROL-D                         | Forward one-half window                                   |
| CONTROL-U                         | Backward one-half window                                  |
| CONTROL-F or PAGE DOWN            | Forward one window                                        |
| CONTROL-B Or PAGE UP              | Backward one window                                       |
| nG                                | To line n (without n, to the last line)                   |
| H                                 | To top of window                                          |
| M                                 | To middle of window                                       |
| L                                 | To bottom of window                                       |

The commands in Table 6-15 enable you to add text to the buffer. All these commands, except r, leave vim in Input mode. You must press ESCAPE to return to
Command mode.

Table 6-15 Adding text
| Command                           | Adds text                                                 |
|-----------------------------------|-----------------------------------------------------------|
| i                                 | Before cursor                                             |
| I                                 | Before first nonblank character on line                   |
| a                                 | After cursor                                              |
| A                                 | At end of line                                            |
| o                                 | Opens a line below current line                           |
| O                                 | Opens a line above current line                           |
| r                                 | Replaces current character (no ESCAPE needed)             |
| R                                 | Replaces characters, starting with current character (overwrite until ESCAPE) |

Table 6-16 lists commands that delete and change text. In this table M is a Unit of Measure that you can precede with a Repeat Factor, n is an optional Repeat Factor, and c is any character.

Table 6-16 Deleting and changing text

| Command | Result                                                                               |
|---------|--------------------------------------------------------------------------------------|
| nX      | Deletes the number of characters specified by n, starting with the current character |
| nX      | Deletes n characters before the current character, starting with the character preceding the current character |
| dM      | Deletes text specified by M |
| ndd     | Deletes n lines             |
| dtc     | Deletes to the next character c on the current line |
| D       | Deletes to end of the line |
| n~      | Changes case of the next n characters |
The following commands leave vim in Input mode. You must press ESCAPE to return to Command mode.
| Command | Result                                                                               |
|---------|--------------------------------------------------------------------------------------|
| nS      | Substitutes m characters                    |
| S       | Substitutes for the entire line |


 Table 6-16 Deleting and changing text (continued)


| Command | Result                                                                               |
|---------|--------------------------------------------------------------------------------------|
| cM      | Changes text specified by M |
| nCC     | Changes lines |
| ctc   | Changes to the next character c on the current line |
| C       | Changes to end of line |

Table 6-17 lists search commands. Here, rexp is a regular expression that can be a simple string of characters.


Table 6-17 Searching
| Command      | Result                                                                               |
|--------------|--------------------------------------------------------------------------------------|
| /rexpRETURN  |        Searches forward for rexp |
| ?rexp RETURN | Searches backward for rexp |
| n            | Repeats original search exactly |
| N            | Repeats original search, in the opposite direction |
| /RETURN      | Original search forward |
| ?RETURN      | Repeats original search backward |
| fc           | Positions the cursor on the next character c on the current line |
| Fc           | Positions the cursor on the previous character c on the current line |
| tc           | Positions the cursor on the character before (to the left of) the next character c on the current line |
| Tc           | Positions the cursor on the character after (to the right of) the previous character c on the current line |
| ;            | Repeats the last f, F. t, or T command |

The syntax of a Substitute command is
```
:[address]s/search-string/replacement-string[/g]
```
where address is one line number or two line numbers separated by a comma. A . (period) represents the current line, $ represents the last line, and % represents the entire file. You can use a marker or a search string in place of a line number. The search-string is a regular expression that can be a simple string of characters. The replacement-string is the replacement string. A g indicates a global replace- ment (more than one replacement per line).


Table 6-18 lists miscellaneous vim commands.

 Table 6-18 Miscellaneous commands
| Command      | Result                                                                               |
|--------------|--------------------------------------------------------------------------------------|
|  J           | Joins the current line and the following line  |
| .            | Repeats the most recent command that made a change |
| :w filename  | Writes the contents of the Work buffer to filename (or to the current file if there is no filename |
|  :q    | Quits vim |
| ZZ     | Writes the contents of the Work buffer to the current file and quits vim |
| :f or CONTROL-G | Displays the filename, status, current line number, number of lines in the Work buffer, and percentage of the Work buffer preceding the current line |
| CONTROL-V | Inserts the next character literally even if it is a vim command (use in Input mode) |

Table 6-19 lists commands that yank and put text. In this table M is a Unit of Mea- sure that you can precede with a Repeat Factor and n is a Repeat Factor. You can precede any of these commands with the name of a buffer using the form "x, where x is the name of the buffer (a–z).

Table 6-19 Yanking and putting text
| Command      | Result                                                                               |
|--------------|--------------------------------------------------------------------------------------|
| yM           | Yanks text specified by M |
| пуу          | Yanks n lines |
| Y            | Yanks to end of line |
| P            | Puts text before or above |
| p            | Puts text after or below |

Table 6-20 lists advanced vim commands.

Table 6-20 Advanced commands

| Command      | Result                                                                               |
|--------------|--------------------------------------------------------------------------------------|
| MX           | Sets marker x, where x is a letter from a to z. |
| '' (two single quotation marks) | Moves cursor back to its previous location. |
| 'x           | Moves cursor to line with marker x. |
| `x           | Moves cursor to character with marker x. |
| :e filename  | Edits filename, requiring you to write changes to the current file (with :w or autowrite) before editing the new file. Use :e! filename to discard changes to the current file. Use :e! without a filename to discard changes to the current file and start editing the saved version of the current file. |
| :n           | Edits the next file when vim is started with multiple filename arguments.  Requires you to write changes to the current file (with :w or autowrite before editing the next file. Use :n! to discard changes to the current file and edit the next file. |
| :rew         | Rewinds the filename list when vim is started with multiple filename arguments and starts editing with the first file. Requires you to write changes to the current file (with w or autowrite) before editing the first file. Use :rew!  to discard changes to the current file and edit the first file. |
| :sh          | Starts a shell. Exit from the shell to return to vim. |
| :! command   | Starts a shell and executes command. |
| !! command   |        Starts a shell, executes command, and places output in the Work buffer, replacing the current line. |

## Exercises

1. How can you cause vim to enter Input mode? How can you make vim revert to Command mode?
2. What is the Work buffer? Name two ways of writing the contents of the Work buffer to the disk.
3. Suppose that you are editing a file that contains the following paragraph and the cursor is on the second tilde (~):
```
         The vim editor has a command, tilde (~),
         that changes lowercase letters to
         uppercase, and vice versa.
         The ~ command works with a Unit of Measure or
         a Repeat Factor, so you can change
        the case of more than one character at a time.
```
How can you
a. Move the cursor to the end of the paragraph?
b. Move the cursor to the beginning of the word Unit? 
c. Change the word character to letter?
4. While working in vim, with the cursor positioned on the first letter of a
word, you give the command x followed by p. Explain what happens.
5. What are the differences between the following commands?
a. i and I b. a and A c. oandO d. r and R e. uandU
6. Which command would you use to search backward through the Work buf- fer for lines that start with the word it?
7. Which command substitutes all occurrences of the phrase this week with the phrase next week?
8. Consider the following scenario: You start vim to edit an existing file. You make many changes to the file and then realize that you deleted a critical section of the file early in your editing session. You want to get that section back but do not want to lose all the other changes you made. What would you do?
9. How can you move the current line to the beginning of the file?
10. Use vim to create the letter_e file of e’s used on page 64. Use as few vim
commands as possible. Which vim commands did you use?

## Advanced Exercises

11. Which commands can you use to take a paragraph from one file and insert it in a second file?
12. Create a file that contains the following list, and then execute commands from within vim to sort the list and display it in two columns. (Hint: Refer to page 940 for more information on pr.)
                         Command mode
                         Input mode
                         Last Line mode
                         Work buffer
                         General-Purpose buffer
                         Named buffer
                         Regular Expression
                         Search String
                         Replacement String
                         Startup File
                         Repeat Factor

13. How do the Named buffers differ from the General-Purpose buffer?
14. Assume that your version of vim does not support multiple Undo com- mands. If you delete a line of text, then delete a second line, and then a third line, which commands would you use to recover the first two lines that you deleted?
15. Which command would you use to swap the words hither and yon on any line with any number of words between them? (You need not worry about special punctuation, just uppercase and lowercase letters and spaces.)