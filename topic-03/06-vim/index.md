# The vim Editor

## Objectives

After studying this topic you should be able to:
* Use vim to create and edit a file
* View vim online help
* Explain the difference between Command and Input modes
* Explain the purpose of the Work buffer
* List the commands that open a line above the cursor, append text to the end of a line, move the cursor to the first line of the file, and move the cursor to the middle line of the screen
* Describe Last Line mode and list some commands that use this mode
* Describe how to set and move the cursor to a marker
* List the commands that move the cursor backward and forward by characters and words
* Describe how to read a file into the Work buffer
* Explain how to search backward and forward for text and how to repeat that search
