# 05 - The Shell

## Objectives

After reading this chapter you should be able to:

* List special characters and methods of preventing the shell from interpreting these characters
* Describe a simple command
* Understand command-line syntax and run commands that include options and arguments
* Explain how the shell interprets the command line
* Redirect output of a command to a file, overwriting the file or appending to it
* Redirect input for a command so it comes from a file
* Connect commands using a pipeline
* Run commands in the background
* Use special characters as wildcards to generate filenames
* Explain the difference between a stand-alone utility and a shell builtin

## Chapter Summary

The shell is the Linux command interpreter. It scans the command line for proper syn- tax, picking out the command name and arguments. The name of the command is argument zero. The first argument is argument one, the second is argument two, and so on. Many programs use options to modify the effects of a command. Most Linux utilities identify an option by its leading one or two hyphens.

When you give it a command, the shell tries to find an executable program with the same name as the command. When it does, the shell executes the program. When it does not, the shell tells you it cannot find or execute the program. If the command is a simple filename, the shell searches the directories listed in the PATH variable to locate the command.

When it executes a command, the shell assigns one file or device to the command’s standard input and another file to its standard output. By default, the shell causes a command’s standard input to come from the keyboard and its standard output to go to the screen. You can instruct the shell to redirect a command’s standard input from or standard output to any file or device. You can also connect standard output of one command to standard input of another command to form a pipeline. A filter is a com- mand that reads its standard input from standard output of one command and writes its standard output to standard input of another command.

When a command runs in the foreground, the shell waits for the command to finish before it displays a prompt and allows you to continue. When you put an ampersand (&) at the end of a command line, the shell executes the command in the background and displays another prompt immediately. Run slow commands in the background when you want to enter other commands at the shell prompt. The jobs builtin dis- plays a list of suspended jobs and jobs running in the background and includes the job number of each.

The shell interprets special characters on a command line to generate filenames. A reference that uses special characters (wildcards) to abbreviate a list of one or more filenames is called an ambiguous file reference. A question mark represents any single character, and an asterisk represents zero or more characters. A single character might also be represented by a character class: a list of characters within brackets.

A builtin is a utility that is built into a shell. Each shell has its own set of builtins. When it runs a builtin, the shell does not fork a new process. Consequently builtins run more quickly and can affect the environment of the current shell.

## UTILITIES AND BUILTINS INTRODUCED IN THIS CHAPTER

Table 5-1 lists the utilities introduced in this chapter.
Table 5-1 New utilities

| Utility                | Function                                                            |
|------------------------|---------------------------------------------------------------------|
| tr                     |    Maps one string of characters to another (page 146)              |
| tee                    |Sends standard input to both a file and standard outout (page 149)   |
| bg                     |    Moves a process to the background (page 151)                     |
| fg                     | Moves a process to the foreground (page 151)                        |
| jobs                   | Displavs a list of suspended jobs and jobs running in the background
(page 152)|

## Exercises
1. What does the shell ordinarily do while a command is executing? What should you do if you do not want to wait for a command to finish before running another command?
2. Using sort as a filter, rewrite the following sequence of commands:
```bash
$ sort list > temp 
$ lpr temp
$ rm temp
```
3. What is a PID number? Why are these numbers useful when you run processes in the background? Which utility displays the PID numbers of the commands you are running?
4. Assume the following files are in the working directory:
```bash
$ ls
intro notesb ref2 section1 section3 section4b 
notesa ref1 ref3 section2 section4a sentrev
```
Give commands for each of the following, using wildcards to express filenames with as few characters as possible.
```
    a. List all files that begin with section.
    b. List the section1, section2, and section3 files only. 
    c. List the intro file only.
    d. List the section1, section3, ref1, and ref3 files.
```

5. Refer to Part VII or the info or man pages to determine which command will
```
a. Display the number of lines in its standard input that contain the word a or A .
b. Display only the names of the files in the working directory that contain
the pattern $(.
c. List the files in the working directory in reverse alphabetical order.
d. Send a list of files in the working directory to the printer, sorted by size.
```

6. Give a command to
```
a. Redirect standard output from a sort command to a file named
phone_list. Assume the input file is named numbers.
b. Translate all occurrences of the characters [ and { to the character (, and all occurrences of the characters ] and } to the character ), in the file permdemos.c. (Hint: Refer to tr on page 1014.)
c. Create a file named book that contains the contents of two other files: part1 and part2.
```

7. The lpr and sort utilities accept input either from a file named on the command line or from standard input.
```
a. Name two other utilities that function in a similar manner.
b. Name a utility that accepts its input only from standard input.
8. Give an example of a command that uses grep
a. With both input and output redirected. b. With only input redirected.
c. With only output redirected.
d. Within a pipeline.
```
In which of the preceding cases is grep used as a filter?

9. Explain the following error message. Which filenames would a subsequent ls command display?
```bash
$ ls
abc abd abe abf abg abh
$ rm abc ab*
rm: cannot remove 'abc': No such file or directory
```

## Advanced Exercises
10. When you use the redirect output symbol (>) on a command line, the shell creates the output file immediately, before the command is executed. Demonstrate that this is true.
11. In experimenting with variables, Max accidentally deletes his PATH variable. He decides he does not need the PATH variable. Discuss some of the problems he could soon encounter and explain the reasons for these problems. How could he easily return PATH to its original value?
12. Assume permissions on a file allow you to write to the file but not to delete it.
a. Give a command to empty the file without invoking an editor.
b. Explain how you might have permission to modify a file that you cannot delete.
13. If you accidentally create a filename that contains a nonprinting character, such as a CONTROL character, how can you remove the file?
14. Why does the noclobber variable not protect you from overwriting an existing file with cp or mv?
15. Why do command names and filenames usually not have embedded SPACEs? How would you create a filename containing a SPACE? How would you remove it? (This is a thought exercise, not recommended practice. If you want to experiment, create a file and work in a directory that contains only your experimental file.)
16. Create a file named answer and give the following command: 
```bash
$ > answers.0102 < answer cat
```
Explain what the command does and why. What is a more conventional way of expressing this command?
