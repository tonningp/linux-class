0
00:00:00,000 --> 00:00:07,000
Let's get a brief look at a few of the system monitoring tools that are available on your system.

1
00:00:07,000 --> 00:00:19,500
In the left-hand window here, I am running GNOME system monitor, which gives me a graphical picture of CPU history on top, with one line for each CPU on the system.

2
00:00:19,500 --> 00:00:27,000
This is a system with four cores, each one of which is hyper-threaded, so it appears as eight CPUs.

3
00:00:27,500 --> 00:00:36,500
In the second line, we are seeing memory and swap history, so here is the memory usage in this circular graph here, and swap here.

4
00:00:36,500 --> 00:00:42,000
And then the third, we are looking at network history, we are seeing both sending and receiving.

5
00:00:43,500 --> 00:00:50,000
So, in the window on the top right, I am just going to run ‘top’, which gives basic process monitoring on my system.

6
00:00:50,000 --> 00:01:04,000
And in the bottom window, I am going to run ‘vmstat’, with a command of ‘-a’ to show everything, and then, I am going to run it every two seconds a thousand times, just so it does not stop.

7
00:01:06,000 --> 00:01:25,500
And then, in the lower right here, I am going to run a very CPU-intensive task, which is a ‘kernel compile’, which proceeds in parallel on each CPU, so it is very CPU-intensive. So, let me do that. So, that is running now.

8
00:01:25,500 --> 00:01:38,000
Then, first of all, you will quickly see that the CPU usage is going to pin up at the top here, so it takes a few seconds to cut in, but then it is going, all the CPUs are busy at the top here.

9
00:01:38,000 --> 00:01:51,500
And also, I can see that in the ‘top’ output, the system is using 80% of its CPU time roughly on user processes, that means compiling and the user space.

10
00:01:51,500 --> 00:01:58,000
And about 10% doing system activity, which is going to be handling system calls, for the most part.

11
00:01:58,000 --> 00:02:08,500
If I hit the number one, I can get a readout for each CPU, instead of just the sum totals, which we now see on the top. We see they are all quite busy.

12
00:02:08,500 --> 00:02:22,000
And on the bottom here, we can see all the activity going on with the memory on the system. In particular, how many pages are active, inactive, swaps coming in and out, how much space is free, etc.

13
00:02:22,500 --> 00:02:26,500
So, that is just a brief look at a few of the tools that are available on your system.

