We are going to launch a graphical program from a terminal window so that one can no longer type in the window. gedit is an easy choice, but you can substitute any other program that does this.

    Open gedit on a new file as in:

    $ gedit somefile     

    While you can still type in the terminal window, the shell will not pay attention to what you input.
    While your pointer is over the terminal window, hit CTRL-Z.

    ^Z
    [3]+  Stopped                 gedit somefile

    You can no longer type in the gedit window.
    With jobs -l, see what processes have been launched from this terminal window:

    $ jobs -l
    [1]  17705 Running                 evince *pdf &
    [2]- 18248 Running                 emacs /tmp/hello.tex &
    [3]+ 19827 Stopped                 gedit somefile     
    Now put the most recent job (gedit somefile) in the background:

    $ bg
    [3]+ gedit somefile &

    Now you should be able to type in the gedit window.
    Put the process in foreground again:

    $ fg
    gedit somefile

    Note you once again input to the terminal window. It has no effect.
    To clean up, suspend the process again and then use kill to terminate it:

    ^Z
    [3]+  Stopped                 gedit somefile
    $ jobs -l 
    [1]  17705 Running                 evince *pdf
    [2]- 18248 Running                 emacs /tmp/hello.tex
    [3]+ 19827 Stopped                 gedit somefile
    $ kill -9 19827
    $ jobs -l
    [1]  17705 Running                 evince *pdf &
    [2]- 18248 Running                 emacs /tmp/hello.tex &
    [3]+ 19827 Killed                  gedit somefile
    $ jobs -l
    [1]- 17705 Running                 evince *pdf &
    [2]- 18248 Running                 emacs /tmp/hello.tex &

