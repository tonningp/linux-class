0
00:00:00,000 --> 00:00:14,000
So now, let's show how to see what the niceness or priority of a process is, and how you can change it with a ‘renice’ command, or how you can do it actually from a graphical user interface.

1
00:00:15,000 --> 00:00:23,500
Let's look at the processes in this current screen with the ‘ps’ command, which we will discuss in detail later.

2
00:00:23,500 --> 00:00:45,000
And we see this current ‘bash’ shell is ‘3077’. To see the priority, I can run ‘ps’ with the options ‘lf’, and there, you see ‘3077’, here is the ‘bash’ command, and the niceness is ‘0’.

3
00:00:45,000 --> 00:01:03,500
So, let's try to change the niceness. I can do ‘renice +5 3077’, and you see, it did change the priority to ‘5’, which I can see once again with the ‘ps’ command. It has made it ‘5’ here.

4
00:01:04,000 --> 00:01:10,500
Notice, that the child process I ran off there, the command ‘ps lf’, also has a niceness of ‘5’.

5
00:01:11,000 --> 00:01:15,000
So, anything else I create in this shell will have that new niceness.

6
00:01:16,000 --> 00:01:21,500
So, this is a lower priority, remember - increasing the niceness lowers the priority.

7
00:01:22,000 --> 00:01:29,000
Now, suppose I try to increase the priority by decreasing the niceness to ‘-5’ [‘renice -5 3077’].

8
00:01:29,000 --> 00:01:37,000
You notice it says I cannot do that. That is because only the superuser or root is allowed to increase the priority of a process.

9
00:01:37,500 --> 00:01:46,000
But, if I type the same command over again with ‘sudo’, it works just fine. And, I can verify that once again with ‘ps lf’.

10
00:01:47,000 --> 00:01:56,500
And there, you see, it is set up to be ‘-5’ now, as well as the new ‘ps’ command that I issued.

11
00:01:56,500 --> 00:02:06,500
Now, if I want to do this from a graphical utility, I can run ‘gnome-system-monitor’. I will start from the command line here, though I could get it from a menu.

12
00:02:07,000 --> 00:02:13,000
And, this is on the first possible screen here on “Processes”.

13
00:02:13,500 --> 00:02:34,000
So, if I hear that was process ‘3077’, and it is right here. And you notice it says, "Priority high", I could right click on there, and I can go to “Change Priority”, and then, I can just say “Low”, and now you see that it is “Low”.

14
00:02:35,000 --> 00:02:50,500
And if I get out of the graphical utility, and I look at the niceness again, you see now, the priority has come up to ‘5’... the niceness of ‘5’, so that means it is a low priority again.

15
00:02:51,000 --> 00:02:57,000
So, that is how you can manipulate the priority of a process from the command line or from a graphical utility.

