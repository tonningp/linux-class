0
00:00:01,610 --> 00:00:08,245
The "top" utility is used to interactively monitor what processes and tasks are doing on your system.

1
00:00:08,676 --> 00:00:10,978
To start it up, all you have to do is type "top".

2
00:00:13,556 --> 00:00:20,419
And it will refresh itself at some interval of, I think by default, two or three seconds, it's easily changed.

3
00:00:21,255 --> 00:00:31,391
It shows you standard information about each process, similar to what you get with the "ps" command, such as the process ID, the user, their priority and niceness,

4
00:00:31,391 --> 00:00:43,623
some information about how the memory is being used by the process, the state, meaning sleeping for instance and, by default, it is sorted by CPU time.

5
00:00:43,788 --> 00:00:47,848
And then there's also memory, the total time and what the actual command is.

6
00:00:48,398 --> 00:00:54,175
The lines at the top have been designed over the years to convey a lot of information.

7
00:00:54,588 --> 00:01:07,982
So on the first one, you see how long the system has been up, how many users there are, the load average or loading averages of how much time has been spent in the last, I believe, one minute, five minutes, etc.

8
00:01:08,340 --> 00:01:14,453
There are currently 295 tests running on the system, but really only one is running, the others are sleeping.

9
00:01:15,948 --> 00:01:27,195
And this shows you the CPU time percentages used by the user processes, by the system, by so-called nice processes, and then idle.

10
00:01:27,855 --> 00:01:33,874
And how many would be waiting, how many are high priority and some other details like that.

11
00:01:34,167 --> 00:01:43,091
Then you have basic information about the memory such as how much is being used, how much swap there is available or being used, how much is in cache, etc.

12
00:01:43,440 --> 00:01:50,853
If I type the letter one, I get statistics for each CPU instead of just the amount that made the total.

13
00:01:51,284 --> 00:01:52,856
And that can be rather useful.

14
00:01:53,342 --> 00:01:54,750
One will take it back again.

15
00:01:55,144 --> 00:02:02,185
If I hit H, I get a listing of what the possible keys I can hit are and what they do.

16
00:02:02,772 --> 00:02:13,461
It wouldn't make sense to try to run through much of that here but I highly recommend that you run "top" and play with these different keys and try to understand what kind of information you can extract.

17
00:02:14,479 --> 00:02:17,090
Well, "top" is how you can do it from the command line.

18
00:02:17,090 --> 00:02:19,811
And if you hit Q that will kill the program.

19
00:02:20,177 --> 00:02:24,879
You can get similar information by going to your menus and finding System Monitor.

20
00:02:25,668 --> 00:02:29,003
And then let me drag that over to this screen.

21
00:02:29,957 --> 00:02:34,825
The processes pane on here is pretty much the same as top's, got the same information.

22
00:02:35,237 --> 00:02:41,470
It's easier to do things like re-sort because I could just click on Memory and get it sorted by how much memory is used.

23
00:02:41,910 --> 00:02:48,521
If I click again, I'll get it in a descending order; priorities, CPU, etc.

24
00:02:48,750 --> 00:02:54,144
You can also, once again, change the priority of processes, you can kill them, etc.

25
00:02:54,144 --> 00:02:55,760
So you can do a lot.

26
00:02:55,760 --> 00:02:58,414
And if you want to see the graph, just click on Resources.

27
00:02:58,937 --> 00:03:11,315
You can see what's going with a constantly redrawn graph and filesystems, you can see how much of your filesystems on the computer are actually full, were they mounted, etc.

28
00:03:11,654 --> 00:03:15,675
So that's a brief explanation of "top" and some related issues.

