0
00:00:02,550 --> 00:00:09,544
The "ps" utility is used very often at the command line to gather information about what is running on the system.

1
00:00:09,924 --> 00:00:18,196
About the various processes and tasks which are running, what resources they are using, what relationship they have to each other, etc.

2
00:00:18,709 --> 00:00:27,621
If I just type "ps" without any arguments, I get the processes which have been launched underneath this particular terminal window.

3
00:00:27,929 --> 00:00:32,617
And in order to make it more interesting, I have a background processes running.

4
00:00:32,617 --> 00:00:38,577
I have a cat, a nautilus which is the file manager, and evince which is the PDF viewer.

5
00:00:39,069 --> 00:00:48,184
I can get more information if I type "ps -f", which you see now also tells me the parent process ID.

6
00:00:48,841 --> 00:01:03,274
So you notice bash has a process ID of 2942, and that's the parent process for both the cat and nautilus, which I launched directly out of the shell, as well as the "ps" command itself.

7
00:01:04,218 --> 00:01:13,197
The "evince" command actually has something else as a parent for complicated reasons having to do with how the GNOME shell runs.

8
00:01:13,577 --> 00:01:18,510
If I want to get a different piece of information, I can do "ps -l".

9
00:01:21,496 --> 00:01:27,592
And now, I am also seeing information in these two columns about the priority.

10
00:01:27,858 --> 00:01:33,738
And a niceness which on most Linux systems is just a numerical shift.

11
00:01:35,749 --> 00:01:43,457
A neutral niceness of zero means the default priority of 80, which is what all normal processes start out with.

12
00:01:44,708 --> 00:01:48,740
Now I can do more than this if I want to give additional arguments.

13
00:01:48,740 --> 00:01:53,783
So I do "ps -elf".

14
00:01:55,168 --> 00:02:01,253
This will tell me about all processes on the system and let me type it into "less", so it doesn't go too quickly.

15
00:02:02,053 --> 00:02:05,574
So you can see this is everything which is actually running on my system.

16
00:02:06,251 --> 00:02:13,461
You'll notice that the first bunch of processes in this rather long list all have these square brackets around them.

17
00:02:13,953 --> 00:02:16,891
That means they were not started by a user program.

18
00:02:16,891 --> 00:02:30,634
They're running inside the kernel to do various kinds of background tasks that run in the kernel all the time, handle moving things from one CPU to another, for instance, and all kinds of other things.

19
00:02:32,081 --> 00:02:34,338
And you'll see they have different priorities.

20
00:02:34,718 --> 00:02:38,771
They have -20 here, -20 there.

21
00:02:38,771 --> 00:02:41,158
That means they're higher priorities.

22
00:02:42,245 --> 00:02:46,923
It's kind of backwards the lower the priority number here, the higher the priority is.

23
00:02:47,641 --> 00:02:59,691
If I page down to the bottom, I will eventually get to the tasks which are not running inside the kernel, which are running, for instance, under mine name here, coop.

24
00:02:59,896 --> 00:03:05,235
As you see, these are the actual user processes and they're quite a bit different.

25
00:03:05,800 --> 00:03:15,888
Now, one reason the "ps" command can be a little confusing is that there are different kinds of options, those which have a dash and those which do not.

26
00:03:15,888 --> 00:03:21,043
So, for instance, "ps -e" is different than "ps [space] e".

27
00:03:21,648 --> 00:03:27,599
One common set of options is "ps aux" without a dash.

28
00:03:28,830 --> 00:03:35,755
And there you see one nice additional piece of information is the percentage of CPU being used.

29
00:03:35,755 --> 00:03:41,297
And of course there isn't much going on in the system right now, so that tends to be zero.

30
00:03:42,364 --> 00:03:47,325
So you can customize what the output of the "ps" command is.

31
00:03:48,597 --> 00:03:54,695
If you look at the man page for "ps", you'll see there are ways to have it print out only the columns that you want.

32
00:03:54,695 --> 00:04:04,300
If you want to produce some customized reports, etc., but a standard Linux System Administrator probably uses "ps" every day for one thing or another.

