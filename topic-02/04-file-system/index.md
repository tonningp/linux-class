# Chapter 04 File Systems

## OBJECTIVES

After completing this unit you should be able to:

* Define hierarchical filesystem, ordinary file, directory file, home directory, working directory, and parent directory
* List best practices for filenames
* Determine the name of the working directory
* Explain the difference between absolute and relative pathnames
* Create and remove directories
* List files in a directory, remove files from a directory, and copy and move files between directories
* List and describe the uses of standard Linux directories and files
* Display and interpret file and directory ownership and permissions
* Modify file and directory permissions
* Describe the uses, differences, and methods of creating hard links and symbolic links

## Summary

CHAPTER SUMMARY

Linux has a hierarchical, or treelike, file structure that makes it possible to organize files so you can find them quickly and easily. The file structure contains directory files and ordinary files. Directories contain other files, including other directories; ordinary files generally contain text, programs, or images. The ancestor of all files is the root directory and is represented by / standing alone or at the left end of a pathname.

Most Linux filesystems support 255-character filenames. Nonetheless, it is a good idea to keep filenames simple and intuitive. Filename extensions can help make filenames more meaningful.

When you are logged in, you are always associated with a working directory. Your home directory is the working directory from the time you log in until you use ed to change directories.

An absolute pathname starts with the root directory and contains all the filenames that trace a path to a given file. The pathname starts with a slash, representing the root directory, and contains additional slashes following each of the directories in the path, except for the last directory in the case of a path that points to a directory file.

A relative pathname is similar to an absolute pathname but traces the path starting from the working directory. A simple filename is the last element of a pathname and is a form of a relative pathname; it represents a file in the working directory.

A Linux filesystem contains many important directories, including **/usr/bin**, which stores most of the Linux utilities, and **/dev**, which stores device files, many of which represent physical pieces of hardware. An important standard Linux file is etc/passwd; it contains information about users, such as a user's ID and full name.

Among the attributes associated with each file are access permissions. They determine who can access the file and how the file may be accessed. Three groups of users can potentially access the file: the owner, the members of a group, and all other users.

An ordinary file can be accessed in three ways: read, write, and execute. The **ls** utility with the -I option displays these permissions. For directories, execute access is redefined to mean that the directory can be searched.

The owner of a file or a user working with root privileges can use the chmod utility to change the access permissions of a file. This utility specifies read, write, and execute permissions for the file's owner, the group, and all other users on the system.

ACLs (Access Control Lists) provide finer-grained control over which users can access specific directories and files than do traditional permissions. Using ACLs you can specify the ways in which each of several users can access a directory or file. Few utilities preserve ACLs when working with files.

An ordinary file stores user data, such as textual information, programs, or images.  A directory is a standard-format disk file that stores information, including names, about ordinary files and other directory files. An inode is a data structure, stored on disk, that defines a file's existence and is identified by an inode number. A directory relates each of the filenames it stores to an inode.

A link is a pointer to a file. You can have several links to a file so you can share the file with other users or have the file appear in more than one directory. Because only one copy of a file with multiple links exists, changing the file through any one link causes the changes to appear in all the links. Hard links cannot link directories or span filesystems, whereas symbolic links can.

   Table 4-3 summarizes the utilities introduced in this chapter.

| Utility | Function                                                   |
|---------|------------------------------------------------------------|
|cd       | Associates you with another working directory (page 94)    |
|chmod    | Changes access permissions on a file (page 102)            |
|getfacl  | Displays a file’s ACL (page 108)                           |
|ln | Makes a link to an existing file (page 113)                      |
|mkdir | Creates a directory (page 93)                                 |
|pwd   |Displays the pathname of the working directory (page 89)       |
|rmdir | Deletes a directory (page 96)                                 |
|setfacl | Modifies a file’s ACL (page 108)                            |

## Exercises

   1. Is each of the following an absolute pathname, a relative pathname, or a simple filename?
          a. milk_co
          b. correspond/business/milk_co
          c. /home/max
          d. /home/max/literature/promo
          e. ..
          f. letter.0210

    2. List the commands you can use to perform these operations:
          a. Make your home directory the working directory
          b. Identify the working directory

    3. If the working directory is /home/max with a subdirectory named literature, give three sets of commands you can use to create a subdirectory named classics under literature. Also give several sets of commands you can use to remove the classics directory and its contents.

    4. The df utility displays all mounted filesystems along with information about each. Use the df utility with the -h (human-readable) option to answer the following questions:
        a. How many filesystems are mounted on the local system?
        b. Which filesystem stores your home directory?
        c. Assuming your answer to exercise 4a is two or more, attempt to create a hard link to a file on another filesystem. What error message is displayed?  What happens when you attempt to create a symbolic link to the file instead?

    5. Suppose you have a file that is linked to a file owned by another user. How can you ensure that changes to the file are no longer shared?

    6. You should have read permission for the /etc/passwd file. To answer the following questions, use cat or less to display /etc/passwd. Look at the fields of information in /etc/passwd for the users on the local system.

          a. Which character is used to separate fields in /etc/passwd?
          b. How many fields are used to describe each user?
          c. How many users are on the local system?
          d. How many different login shells are in use on your system? (Hint: Look at the last field.)
          e. The second field of /etc/ passwd stores user passwords in encoded form.  If the password field contains an x, your system uses shadow passwords and stores the encoded passwords elsewhere. Does your system use shadow passwords?

    7. If /home/zach/draft and /home/max/letter are links to the same file and the following sequence of events occurs, what will be the date in the opening of the letter?

        a. Max gives the command vim letter.
        b. Zach gives the command vim draft.
        c. Zach changes the date in the opening of the letter to January 31, writes the file, and exits from vim.
        d. Max changes the date to February 1, writes the file, and exits from vim.

    8. Suppose a user belongs to a group that has all permissions on a file named jobs_list, but the user, as the owner of the file, has no permissions. Describe which operations, if any, the user/owner can perform on jobs_list. Which command can the user/owner give that will grant the user/owner all permissions on the file?

    9. Does the root directory have any subdirectories you cannot search as an ordinary user? Does the root directory have any subdirectories you cannot read as a regular user? Explain.

    10. Assume you are given the directory structure shown in Figure 4-2 on page 85 and the following directory permissions:

                   d--X--X---     3 zach pubs 512 2018-03-10 15:16 business
                   drwxr-xr-x    2 zach pubs 512 2018-03-10 15:16 business/milk_co

    For each category of permissions--owner, group, and other--what happens when you run each of the following commands? Assume the working directory is the parent of correspond and that the file cheese_co is readable by everyone.

        a. cd correspond/business/milk_co
        b. Is -1 correspond/business
        c. cat correspond/business/cheese_co

    11. What is an inode? What happens to the inode when you move a file within a filesystem?

    12. What does the .. entry in a directory point to? What does this entry point to in the root (/) directory?

    13. How can you create a file named –i? Which techniques do not work, and why do they not work? How can you remove the file named –i?

    14. Suppose the working directory contains a single file named andor. What error message is displayed when you run the following command line?
```bash
    $ mv andor and\/or
```
    Under what circumstances is it possible to run the command without producing an error?

    15.  The ls –i command displays a filename preceded by the inode number of the file (page 115). Write a command to output inode/filename pairs for the files in the working directory, sorted by inode number. (Hint: Use a pipeline.)

    16.  Do you think the system administrator has access to a program that can decode user passwords? Why or why not? (See exercise 6.)

    17.  Is it possible to distinguish a file from a hard link to a file? That is, given a filename, can you tell whether it was created using an ln command? Explain.

    18.  Explain the error messages displayed in the following sequence of commands:
```bash
$ ls -l
drwxrwxr-x. 2 max pubs 1024 03-02 17:57 dirtmp $ ls dirtmp
$ rmdir dirtmp
rmdir: dirtmp: Directory not empty
$ rm dirtmp/*
rm: No match.
```
