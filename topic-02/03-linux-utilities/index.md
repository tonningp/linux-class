# Linux Utilities

## Objectives

    After reading this chapter you should be able to:

* List special characters and methods of preventing the shell from interpreting these characters
* Use basic utilities to list files and display text files
* Copy, move, and remove files
* Search, sort, print, and compare text files
* String commands together using a pipeline
* Compress, decompress, and archive files
* Locate utilities on the system
* Display information about users
* Communicate with other users

[Home](../index.md)