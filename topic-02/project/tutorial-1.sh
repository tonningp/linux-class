#!/usr/bin/env bash 
#

if [[ ${1}x == x ]]
then
    echo "usage: $0 week"
    exit 1
fi
week=${1}
host_base_url="https://boss.vvc.edu/moodle/cis190/content"
week_index="week-${week}-exercises.txt"
exercise_index="${host_base_url}/${week_index}"

full_name=''
student_id=''
function get_student_info() {
    read -p "Enter your first and last name: " full_name
    read -p "Enter your student id: " student_id
}


export txtred=$(tput setf 4) # Red
export txtgrn=$(tput setf 2) # Green
export txtylw=$(tput setf 6) # Yellow
export txtblu=$(tput setf 1) # Blue
export txtpur=$(tput setf 5) # Purple
export txtcyn="\e[36m" # Cyan
export txtwht=$(tput setf 7) # White
export coloroff="\e[0m" # turn color off
export txtreset=$(tput sgr0)

function cleanup() {
    echo "Cleaning up"
    exit 0
}

trap "cleanup" SIGINT

function prompt() {
    read -p "$1" v
    eval "$2=$v"
}

attempt=1
function display_question_prompt() {
    echo "********************************************************************************"
    echo "*                                                                              *" 
    echo "*          Follow the instructions below and perform the given task            *"
    echo "*                 type exit or ^d when you have completed the task             *" 
    echo "*                                                                              *" 
    echo "********************************************************************************"
    echo
    echo "$1"
    echo
}

function main() {
CONTENT=content
get_student_info
full_name_f=$(echo $full_name | tr [A-Z] [a-z] |tr ' ' '_' )
while true
do
    IFS_OLD=$IFS
    IFS=$'\n'
    for l in $(curl -fsSL ${exercise_index})
    do
        export question_prompt=$(curl -fsSL ${host_base_url}/${l})
        script -a -q week-${week}-${full_name_f}-${student_id}-attempt-${attempt}.out bash -c 'source ./startup;exec bash -i'
    done
    IFS=$IFS_OLD

    next=''&&prompt "Attempt # ${attempt} completed.  If you want to do the exercise again press y (then Enter) to continue or n (then Enter) to quit: " next
    echo $next

    if [[ $next == 'n' ]]
    then
        kill -SIGINT $$
    fi
    attempt=$((attempt+1))
done
}


main
