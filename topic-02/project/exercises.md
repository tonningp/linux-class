1. Which commands can you use to determine who is logged in on a specific terminal?

2. How can you keep other users from using write to communicate with you? Why would you want to?

3. Perform the following:

* create a file called to_do using touch
* copy to_do to the file done
* rename to_do to the file done

4. How can you find out which utilities are available on your system for editing files? Which utilities are available for editing on your system?

5. How can you find the phone number for Ace Electronics in a file named phone that contains a list of names and phone numbers? Which command can you use to display the entire file in alphabetical order? How can you dis- play the file without any adjacent duplicate lines? How can you display the file without any duplicate lines?
6. What happens when you use diff to compare two binary files that are not identical? (You can use gzip to create the binary files.) Explain why the diff output for binary files is different from the diff output for ASCII files.
7. Create a .plan file in your home directory. Does finger display the contents of your .plan file?
