[Critical DME List](https://www.faa.gov/air_traffic/flight_info/aeronav/criticaldme/)

A critical Distance Measuring Equipment (DME) is a DME facility that, when not available, results in navigation service which is not sufficient for DME/DME/IRU operations along all or portions of a specific route or procedure. The required performance assumes an aircraft's RNAV system meets the minimum standard (baseline) for DME/DME RNAV systems found in AC 90-100A, appendix 1, or the minimum standard for DME/DME/IRU systems found in AC90-100A, appendix 2. For example, terminal RNAV DPs and STARs may be published with only two (2) DMEs, in which case, both are critical.

