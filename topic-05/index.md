# 09 ssh and scp Utilities

## Objectives

After completing this section you should be able to:

* Copy files and directories using rsync
* Explain why rsync is secure
* Back up files and directories to another system using
```bash
rsync
```
* Explain the effect of including a trailing slash on the name of the source directory
* Use rsync options to delete files in the destination that are not in the source, preserve modification times of copied files, and perform a dry run so rsync takes no action

## Summary
The rsync utility copies an ordinary file or directory hierarchy locally or between the local system and a remote system on a network. By default, this utility uses openSSH to transfer files and the same authentication mechanism as openSSH; therefore, it provides the same security as openSSH. The rsync utility prompts for a password when it needs one.

## Exercises
1. List three features of rsync.
2. Write an rsync command that copies the backmeup directory from your home directory on the local system to the /tmp directory on guava, preserv- ing file ownership, permissions, and modification times. Write a command that will copy the same directory to your home directory on guava. Do not assume the working directory on the local system is your home directory.
3. You are writing an rsync command that includes the – –delete option. Which options would you use to test the command without copying or removing any files?
4. What does the ––archive option do? Why is it useful?
5. When running a script such as bkup (page 698) to back up files on a remote
system, how could you rotate (rename) files on a remote system?
6. What effect does a trailing slash (/) on the source-file have?

## ssh scp Objectives

After completing this section you should be able to:

* Explain the need for encrypted services
* Log in on a remote OpenSSH server system using ssh
* Copy files and directories to and from a remote system securely
* Set up an OpenSSH server
* Configure OpenSSH server options
* Set up a client/server so you do not need to use a password to log in using ssh or scp
* Enable trusted X11 tunneling between a client and an OpenSSH server
* Remove a known host record from the ~/.ssh/known_hosts file
* Enable trusted X11 forwarding
* List the uses of ssh tunneling (port forwarding)

## Summary

OpenSSH is a suite of secure network connectivity tools that encrypts all traffic, including passwords, thereby helping to thwart attackers who might otherwise eavesdrop, hijack connections, and steal passwords. The sshd server daemon accepts connections from clients including ssh (runs a command on or logs in on another system), scp (copies files to and from another system), and sftp (securely replaces ftp). Helper programs including ssh-keygen (creates, manages, and converts authentication keys), ssh-agent (manages keys during a session), and ssh-add (works with ssh-agent) create and manage authentication keys.
To ensure secure communications, when an OpenSSH client opens a connection, it verifies that it is connected to the correct server. Then OpenSSH encrypts communi- cation between the systems. Finally, OpenSSH makes sure the user is authorized to log in on or copy files to and from the server. You can secure many protocols—including POP, X, IMAP, VNC, and WWW—by tunneling them through ssh.
When it is properly set up, OpenSSH also enables secure X11 forwarding. With this feature, you can run securely a graphical program on a remote system and have the display appear on the local system.

## Exercises

1. What is the difference between the scp and sftp utilities?
2. How can you use ssh to find out who is logged in on a remote system named
tiger?
3. How would you use scp to copy your ~/.bashrc file from the system named
plum to the local system?
4. How would you use ssh to run xterm on plum and show the display on the
local system? Your username on plum is max.
5. What problem can enabling compression present when you are using ssh to
run remote X applications on a local display?
6. When you try to connect to a remote system using an OpenSSH client and OpenSSH displays a message warning you that the remote host identification has changed, what has happened? What should you do?

## Advanced Exercises

7. Which scp command would you use to copy your home directory from plum to the local system?
8. Which single command could you give to log in as max on the remote system named plum and open a root shell (on plum)? Assume plum has remote root logins disabled.
9. How could you use ssh to compare the contents of the ~/memos directories on plum and the local system?
10. How would you use rsync along with OpenSSH authentication to copy the memos12 file from the working directory on the local system to your home directory on plum? How would you copy the memos directory from the working directory on the local system to your home directory on plum and cause rsync to display each file as it copied the file?
