# 07 Shell Deep Dive

## Objectives

After reading this chapter you should be able to:

* Describe the purpose and history of bash
* List the startup files bash runs
* Use three different methods to run a shell script
* Understand the purpose of the PATH variable
* Manage multiple processes using job control
* Redirect error messages to a file
* Use control operators to separate and group commands
* Create variables and display the values of variables and parameters
* List and describe common variables found on the system
* Reference, repeat, and modify previous commands using history
* Use control characters to edit the command line
* Create, display, and remove aliases and functions
* Customize the bash environment using the set and shopt builtins
* List the order of command-line expansion

## Chapter Summary

The shell is both a command interpreter and a programming language. As a com- mand interpreter, it executes commands you enter in response to its prompt. As a programming language, it executes commands from files called shell scripts. When you start a shell, it typically runs one or more startup files.
When the file holding a shell script is in the working directory, there are three basic ways to execute the shell script from the command line.
1. Type the simple filename of the file that holds the script.
2. Type an absolute or relative pathname, including the simple filename
preceded by ./.
3. Type bash or tcsh followed by the name of the file.
Technique 1 requires the working directory to be in the PATH variable. Techniques 1 and 2 require you to have execute and read permission for the file holding the script. Technique 3 requires you to have read permission for the file holding the script.
A job is another name for a process running a pipeline (which can be a simple command). You can bring a job running in the background into the foreground using the fg builtin. You can put a foreground job into the background using the bg builtin, provided you first suspend the job by pressing the suspend key (typically CONTROL-Z). Use the jobs builtin to display the list of jobs that are running in the background or are suspended.
The shell allows you to define variables. You can declare and initialize a variable by assigning a value to it; you can remove a variable declaration using unset. Shell vari- ables are local to the process they are defined in. Environment variables are global and are placed in the environment using the export (bash) or setenv (tcsh) builtin so they are available to child processes. Variables you declare are called user-created variables. The shell defines keyword variables. Within a shell script you can work with the positional (command-line) parameters the script was called with.
Locale specifies the way locale-aware programs display certain kinds of data, such as times and dates, money and other numeric values, telephone numbers, and mea- surements. It can also specify collating sequence and printer paper size.
Each process is the execution of a single command and has a unique identification (PID) number. When you give the shell a command, it forks a new (child) process to execute the command (unless the command is built into the shell). While the child process is running, the shell is in a state called sleep. By ending a command line with an ampersand (&), you can run a child process in the background and bypass the sleep state so the shell prompt returns immediately after you press RETURN. Each com- mand in a shell script forks a separate process, each of which might in turn fork other processes. When a process terminates, it returns its exit status to its parent process. An exit status of zero signifies success; a nonzero value signifies failure.
The history mechanism maintains a list of recently issued command lines called events, that provides a way to reexecute previous commands quickly. There are several ways to work with the history list; one of the easiest is to use a command-line editor.
When using an interactive Bourne Again Shell, you can edit a command line and com- mands from the history list, using either of the Bourne Again Shell’s command-line editors (vim or emacs). When you use the vim command-line editor, you start in Input mode, unlike with the stand-alone version of vim. You can switch between Command and Input modes. The emacs editor is modeless and distinguishes commands from editor input by recognizing control characters as commands.
An alias is a name the shell translates into another name or command. Aliases allow you to define new commands by substituting a string for the first token of a simple command. The Bourne Again and TC Shells use different syntaxes to define an alias, but aliases in both shells work similarly.

A shell function is a series of commands that, unlike a shell script, is parsed prior to being stored in memory. As a consequence shell functions run faster than shell scripts. Shell scripts are parsed at runtime and are stored on disk. A function can be defined on the command line or within a shell script. If you want the function definition to remain in effect across login sessions, you can define it in a startup file. Like functions in many programming languages, a shell function is called by giving its name fol- lowed by any arguments.
There are several ways to customize the shell’s behavior. You can use options on the command line when you call bash. You can also use the bash set and shopt builtins to turn features on and off.
When it processes a command line, the Bourne Again Shell replaces some words with expanded text. Most types of command-line expansion are invoked by the
appearance of a special character within a word (for example, the leading dollar sign that denotes a variable). Table 8-6 on page 325 lists these special characters. The expansions take place in a specific order. Following the history and alias expan- sions, the common expansions are parameter and variable expansion, command substitution, and pathname expansion. Surrounding a word with double quotation marks suppresses all types of expansion except parameter and variable expansion. Single quotation marks suppress all types of expansion, as does quoting (escaping) a special character by preceding it with a backslash.

## Exercises

1. Explain the following unexpected result:
$ whereis date
date: /bin/date ...
$ echo $PATH .:/usr/local/bin:/usr/bin:/bin
$ cat > date
echo "This is my own version of date." $ ./date
Sun May 21 11:45:49 PDT 2017
2. What are two ways you can execute a shell script when you do not have execute permission for the file containing the script? Can you execute a shell script if you do not have read permission for the file containing the script?
3. What is the purpose of the PATH variable?
a. Set the PATH variable and place it in the environment so it causes the
shell to search the following directories in order: 

* /usr/local/bin
* /usr/bin
* /bin
* /usr/kerberos/bin
* The bin directory in your home directory 
* The working directory

b. If there is an executable file named doit in /usr/bin and another file with the same name in your ~/bin directory, which one will be executed?
c. If your PATH variable is not set to search the working directory, how can you execute a program located there?
d. Which command can you use to add the directory /usr/games to the end of the list of directories in PATH?
4. Assume you have made the following assignment:
```bash
$ person=zach
```

Give the output of each of the following commands. 
```bash
a. echo $person
b. echo '$person'
c. echo "$person"
```
5. The following shell script adds entries to a file named journal-file in your home directory. This script helps you keep track of phone conversations and meetings.
```bash
$ cat journal
# journal: add journal entries to the file 
# $HOME/journal-file
         file=$HOME/journal-file
         date >> $file
         echo -n "Enter name of person or group: "
         read name
         echo "$name" >> $file
         echo >> $file
         cat >> $file
         echo "----------------------------------------------------" >>
         $file
         echo >> $file
```
a. What do you have to do to the script to be able to execute it?
b. Why does the script use the read builtin the first time it accepts input from
the terminal and the cat utility the second time?
6. Assume the /home/zach/grants/biblios and /home/zach/biblios directories exist. Specify Zach’s working directory after he executes each sequence of commands. Explain what happens in each case.
```bash
a. $pwd /home/zach/grants
$ CDPATH=$(pwd) $ cd
$ cd biblios
b. $pwd /home/zach/grants
$ CDPATH=$(pwd)
$ cd $HOME/biblios
```
7. Name two ways you can identify the PID number of the login shell.

8. Enter the following command:
```bash
$ sleep 30 | cat /etc/services
```
Is there any output from sleep? Where does cat get its input from? What has to happen before the shell will display a prompt?

## Advanced Exercises

9. Write a sequence of commands or a script that demonstrates variable expansion occurs before pathname expansion.
10. Write a shell script that outputs the name of the shell executing it.
11. Explain the behavior of the following shell script:
```bash
$ cat quote_demo twoliner="This is line 1. This is line 2."
echo "$twoliner"
echo $twoliner
```
a. How many arguments does each echo command see in this script? Explain.
b. Redefine the IFS shell variable so the output of the second echo is the same as the first.
12. Add the exit status of the previous command to your prompt so it behaves similarly to the following:
```bash
$ [0] ls xxx
ls: xxx: No such file or directory $ [1]
```
13. The dirname utility treats its argument as a pathname and writes to standard output the path prefix—that is, everything up to but not including the last component:
```bash
$ dirname a/b/c/d a/b/c
```
If you give dirname a simple filename (no / characters) as an argument, dirname writes a . to standard output:
```bash
$ dirname simple .
```
Implement dirname as a bash function. Make sure it behaves sensibly when given such arguments as /.
14. Implement the basename utility, which writes the last component of its pathname argument to standard output, as a bash function. For example, given the pathname a/b/c/d, basename writes d to standard output:
```bash
$ basename a/b/c/d d
```
15. The Linux basename utility has an optional second argument. If you give the command basename path suffix, basename removes the suffix and the prefix from path:
```bash
$ basename src/shellfiles/prog.bash .bash prog
$ basename src/shellfiles/prog.bash .c prog.bash
```
Add this feature to the function you wrote for exercise 14.
