# 08 Shell Programming

## Objectives

After reading this chapter you should be able to:

* Use control structures to implement decision making and repetition in shell scripts
* Handle input to and output from scripts
* Use shell variables (local) and environment variables
*global)
* Evaluate the value of numeric variables
* Use bash builtin commands to call other scripts inline, trap signals, and kill processes
* Use arithmetic and logical expressions
* List standard programming practices that result in well-written scripts

## Summary

The shell is a programming language. Programs written in this language are called shell scripts, or simply scripts. Shell scripts provide the decision and looping control structures present in high-level programming languages while allowing easy access to system utilities and user programs. Shell scripts can use functions to modularize and simplify complex tasks.

The control structures that use decisions to select alternatives are if...then, if...then...else, and if...then...elif. The case control structure provides a multiway branch and can be used when you want to express alternatives using a simple pattern-
matching syntax.

The looping control structures are for...in, for, until, and while. These structures perform one or more tasks repetitively.

The break and continue control structures alter control within loops: break trans- fers control out of a loop, and continue transfers control immediately to the top of a loop.

The Here document allows input to a command in a shell script to come from within the script itself.

The Bourne Again Shell provides the ability to manipulate file descriptors. Coupled with the read and echo builtins, file descriptors allow shell scripts to have as much control over input and output as do programs written in lower-level languages.

By default, variables are local to the process they are declared in; these variables are called shell variables. You can use export to cause variables to be environment variables, which are available to children of the process they are declared in.

The declare builtin assigns attributes, such as readonly, to bash variables. The Bourne Again Shell provides operators to perform pattern matching on variables, provide default values for variables, and evaluate the length of variables. This shell also supports array variables and local variables for functions and provides built-in integer arithmetic, using the let builtin and an expression syntax similar to that found in the C programming language.

Bourne Again Shell builtins include type, read, exec, trap, kill, and getopts. The type builtin displays information about a command, including its location; read allows a script to accept user input.

The exec builtin executes a command without creating a new process. The new com- mand overlays the current process, assuming the same environment and PID number of that process. This builtin executes user programs and other Linux commands when it is not necessary to return control to the calling process.

The trap builtin catches a signal sent to the process running the script and allows you to specify actions to be taken upon receipt of one or more signals. You can use this builtin to cause a script to ignore the signal that is sent when the user presses the interrupt key.

The kill builtin terminates a running program. The getopts builtin parses command-line arguments, making it easier to write programs that follow standard Linux/macOS conventions for command-line arguments and options.

In addition to using control structures, builtins, and functions, shell scripts generally call Linux/macOS utilities. The find utility, for instance, is commonplace in shell scripts that search for files in the system hierarchy and can perform a wide range of tasks.

There are two basic types of expressions: arithmetic and logical. Arithmetic expressions allow you to do arithmetic on constants and variables, yielding a numeric result. Logical (Boolean) expressions compare expressions or strings, or test conditions, to yield a true or false result. As with all decisions within shell scripts, a true status is represented by the value 0; false, by any nonzero value.

A well-written shell script adheres to standard programming practices, such as specifying the shell to execute the script on the first line of the script, verifying the number and type of arguments that the script is called with, displaying a standard usage message to report command-line errors, and redirecting all informational messages to standard error

## Exercises

1. Rewrite the journal script of Chapter 8 (exercise 5, page 377) by adding commands to verify that the user has write permission for a file named journal-file in the user’s home directory, if such a file exists. The script should take appropriate actions if journal-file exists and the user does not have write permission to the file. Verify that the modified script works.
2. The special parameter "$@" is referenced twice in the out script (page 436). Explain what would be different if the parameter "$*" were used in its place.
3. Write a filter that takes a list of files as input and outputs the basename (page 459) of each file in the list.
4. Write a function that takes a single filename as an argument and adds execute permission to the file for the user.
a. When might such a function be useful?
b. Revise the script so it takes one or more filenames as arguments and adds execute permission for the user for each file argument.
c. What can you do to make the function available every time you log in?
d. Suppose that, in addition to having the function available on subsequent login sessions, you want to make the function available in your current shell. How would you do so?
5. When might it be necessary or advisable to write a shell script instead of a shell function? Give as many reasons as you can think of.
6. Write a shell script that displays the names of all directory files, but no other types of files, in the working directory.
7. Write a script to display the time every 15 seconds. Read the date man page and display the time, using the %r field descriptor. Clear the window (using the clear command) each time before you display the time.
8. Enter the following script named savefiles, and give yourself execute permission to the file:
```bash
$ cat savefiles
#! /bin/bash
echo "Saving files in working directory to the file savethem." exec > savethem
for i in *
do
                  echo
          "==================================================="
                  echo "File: $i"
                  echo
          "==================================================="
cat "$i" done
```
a. Which error message do you receive when you execute this script? Rewrite the script so that the error does not occur, making sure the output still goes to savethem.
b. What might be a problem with running this script twice in the same directory? Discuss a solution to this problem.
9. Read the bash man or info page, try some experiments, and answer the following questions:
a. How do you export a function?
b. What does the hash builtin do?
c. What happens if the argument to exec is not executable?
10. Using the find utility, perform the following tasks:
a. List all files in the working directory and all subdirectories that have been
modified within the last day.
b. List all files you have read access to on the system that are larger than 1 megabyte.
c. Remove all files named core from the directory structure rooted at your home directory.
d. List the inode numbers of all files in the working directory whose filenames end in .c.
e. List all files you have read access to on the root filesystem that have been modified in the last 30 days.
11. Write a short script that tells you whether the permissions for two files, whose names are given as arguments to the script, are identical. If the per- missions for the two files are identical, output the common permission field. Otherwise, output each filename followed by its permission field. (Hint: Try using the cut utility.)
12. Write a script that takes the name of a directory as an argument and searches the file hierarchy rooted at that directory for zero-length files. Write the names of all zero-length files to standard output. If there is no option on the command line, have the script delete the file after displaying its name, asking the user for confirmation, and receiving positive confirmation. A –f (force) option on the command line indicates that the script should display the filename but not ask for confirmation before deleting the file.

## Advanced Exercises

13. Write a script that takes a colon-separated list of items and outputs the items, one per line, to standard output (without the colons).
14. Generalize the script written in exercise 13 so the character separating the list items is given as an argument to the function. If this argument is absent, the separator should default to a colon.
15. Write a function named funload that takes as its single argument the name of a file containing other functions. The purpose of funload is to make all functions in the named file available in the current shell; that is, funload loads the functions from the named file. To locate the file, funload searches the colon-separated list of directories given by the environment variable FUNPATH. Assume the format of FUNPATH is the same as PATH and the search of FUNPATH is similar to the shell’s search of the PATH variable.
16. Rewrite bundle (page 463) so the script it creates takes an optional list of filenames as arguments. If one or more filenames are given on the command line, only those files should be re-created; otherwise, all files in the shell archive should be re-created. For example, suppose all files with the file- name extension .c are bundled into an archive named srcshell, and you want to unbundle just the files test1.c and test2.c. The following command will unbundle just these two files:
```bash
$ bash srcshell test1.c test2.c
```
17. Which kind of links will the lnks script (page 439) not find? Why?
18. In principle, recursion is never necessary. It can always be replaced by an iterative construct, such as while or until. Rewrite makepath (page 514) as a nonrecursive function. Which version do you prefer? Why?
19. Lists are commonly stored in environment variables by putting a colon ( : ) between each of the list elements. (The value of the PATH variable is an example.) You can add an element to such a list by catenating the new element to the front of the list, as in
```bash
    PATH=/opt/bin:$PATH
```
If the element you add is already in the list, you now have two copies of it in the list. Write a shell function named addenv that takes two arguments: (1) the name of a shell variable and (2) a string to prepend to the list that is the value of the shell variable only if that string is not already an element of the list. For example, the call
```bash
    addenv PATH /opt/bin
```
would add /opt/bin to PATH only if that pathname is not already in PATH. Be sure your solution works even if the shell variable starts out empty. Also make sure you check the list elements carefully. If /usr/opt/bin is in PATH but /opt/bin is not, the example just given should still add /opt/bin to PATH. (Hint: You might find this exercise easier to complete if you first write a function locate_field that tells you whether a string is an element in the value of a variable.)
20. Write a function that takes a directory name as an argument and writes to standard output the maximum of the lengths of all filenames in that directory. If the function’s argument is not a directory name, write an error message to standard output and exit with nonzero status.
21. Modify the function you wrote for exercise 20 to descend all subdirectories of the named directory recursively and to find the maximum length of any filename in that hierarchy.
22. Write a function that lists the number of ordinary files,directories,blockspecial files, character special files, FIFOs, and symbolic links in the working directory. Do this in two different ways:
a. Use the first letter of the output of ls –l to determine a file’s type.
b. Use the file type condition tests of the [[ expression ]] syntax to determine
a file’s type.
23. Modify the quiz program (page 521) so that the choices for a question are randomly arranged.
