# Objective

## Learn about the components that make up a Linux distribution

What is necessary to know for this course is that it focuses on one of the three major Linux distribution families that currently exist.  That distribution is **Ubuntu**.

For a rather long list of available distributions, see The [LWN.net](https://lwn.net/Distributions/) Linux Distribution List.

![Alt Text](../../media/01-introduction/chapter01_The_Linux_Kernel_Distribution_Families_and_Individual_Distributions.png)