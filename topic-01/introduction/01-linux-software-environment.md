The material produced by The Linux Foundation is distribution-flexible. This means that technical explanations, labs, and procedures should work on almost all modern distributions. While choosing between available Linux systems, you will notice that the technical differences are mainly about package management systems, software versions, and file locations. Once you get a grasp of those differences, it becomes relatively painless to switch from one Linux distribution to another.

The desktop environment used for this course is GNOME. As we will note in Chapter 4: Graphical Interface, there are different environments, but we selected GNOME as it is the most widely used.

