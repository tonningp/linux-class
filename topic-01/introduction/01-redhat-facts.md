Some of the key facts about the Red Hat distribution family are:

            Fedora serves as an upstream testing platform for RHEL.
            CentOS is a close clone of RHEL, while Oracle Linux is mostly a copy with some changes (in fact, CentOS has been part of Red Hat since 2014).
            A heavily patched version 3.10 kernel is used in RHEL/CentOS 7, while version 4.18 is used in RHEL/CentOS 8.
            It supports hardware platforms such as Intel x86, Arm, Itanium, PowerPC, and IBM System z.
            It uses the yum and dnf RPM-based yum package managers (covered in detail later) to install, update, and remove packages in the system.
            RHEL is widely used by enterprises which host their own systems.
