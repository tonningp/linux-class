The Linux Foundation training is for the community and is designed by members of the community. To get more information about specific courses offered by the Linux Foundation visit our catalog:

* System Administration courses
* Cloud & Containers courses
* Networking courses
* Blockchain courses
* Linux Kernel Development courses
* IoT & Embedded Development courses
* System Engineering courses
* DevOps & Site Reliability courses
* Open Source Best Practice courses
* AI/Machine Learning courses.

The Linux Foundation training is distribution-flexible, technically advanced, and created with the actual leaders of the open source development community themselves. Most courses are more than 50% focused on hands-on labs and activities to build real world skills.
