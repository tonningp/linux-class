# 01 - What is an Operating System?

Wikipedia -

"An operating system (OS) is system software that manages computer hardware, software resources, and provides common services for computer programs." [Wikipedia Article](https://en.wikipedia.org/wiki/Operating_system)

Operating Systems Make Computer Hardware usable!

Where does it fit?

![Alt Text](https://upload.wikimedia.org/wikipedia/commons/e/e1/Operating_system_placement.svg)

Where does the Kernel Fit?
![SVG from wikimedia showing diagram of components of an operating system](https://upload.wikimedia.org/wikipedia/commons/8/8f/Kernel_Layout.svg)

Unix is an early Operating System
![Alt Text](https://upload.wikimedia.org/wikipedia/commons/7/77/Unix_history-simple.svg)
[Get Image](https://upload.wikimedia.org/wikipedia/commons/7/77/Unix_history-simple.svg)

## Summary

The Linux operating system grew out of the UNIX heritage to become a popular alternative to traditional systems (that is, Windows) available for microcomputer (PC) hardware. UNIX users will find a familiar environment in Linux. Distributions of Linux contain the expected complement of UNIX utilities, contributed by programmers around the world, including the set of tools developed as part of the GNU Project. The Linux community is committed to the continued development of this system. Support for new microcomputer devices and features is added soon after the hardware becomes available, and the tools available on Linux continue to be refined. Given the many commercial software packages available to run on Linux platforms and the many hardware manufacturers offering Linux on their systems, it is clear that the system has evolved well beyond its origin as an undergraduate project to become an operating system of choice for academic, commercial, professional, and personal use.

![Tux](https://upload.wikimedia.org/wikipedia/commons/3/35/Tux.svg)

[Next - Linux Distributions](01-linux-distributions.md)

[Home](../index.md)