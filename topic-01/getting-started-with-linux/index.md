# The Linux Operating System

## [What is Linux](what-is-linux.md)

## Lab Environment

What you will need:

1. A computer with Windows 10 or higher, MacOS or Linux
2. ***Windows Computer Users*** If you have Windows 10 or higher, it is recommended that you install the [Windows terminal](https://learn.microsoft.com/en-us/windows/terminal/) Click on the [Install Windows Terminal, button](https://aka.ms/terminal).  If you want to work with Linux on your local computer, then you should install the [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/en-us/windows/wsl/install)
3. ***Mac Users*** If you have a Mac, then you will want to install the [Iterm2 terminal](https://iterm2.com/).
4. ***Linux Users*** You have everything you need ;-)