# Summary

1. What is free software? List three characteristics of free software.
1.1 Free software is software that users are free to use, modify, and distribute. It is often developed by a community of volunteers and is distributed under a license that allows users to access and modify the source code. Some characteristics of free software include:

    * Users are free to run the software for any purpose.
    * Users are free to modify the software to suit their needs.
    * Users are free to distribute copies of the software to others.
2. Why is Linux popular? Why is it popular in academia?
    2.1 Linux is a free and open-source operating system that is based on the Unix operating system. It is popular for a number of reasons, including:

    * It is highly customizable and can be tailored to specific needs.
    * It is stable, reliable, and secure.
    * It has a large and active community of developers and users who contribute to and support the development of the operating system.

    Linux is also popular in academia because it is free and open-source, which makes it an attractive option for researchers and students who may not have access to expensive proprietary software. It is also widely used in research and education because of its flexibility, stability, and security.
3. What are multiuser systems? Why are they successful?
    3.1 Multiuser systems are computer systems that allow multiple users to access and use the system simultaneously. They are successful because they allow multiple users to share the resources of a single computer, which can be more cost-effective than having each user have their own dedicated computer. In addition, multiuser systems can provide improved security, as the operating system can enforce access controls to prevent unauthorized users from accessing sensitive resources. They are also useful in environments where users need to collaborate or share resources, as they allow multiple users to work on the same system at the same time.
4. What is the Free Software Foundation/GNU? What is Linux? Which parts of the Linux operating system did each provide? Who else has helped build and refine this operating system?

    The Free Software Foundation (FSF) is a nonprofit organization that promotes the use of free software and defends the freedoms of computer users. The FSF was founded in 1985 by Richard Stallman, who is also the creator of the GNU (GNU's Not Unix) operating system.

    Linux is a free and open-source operating system that is based on the Unix operating system. It was first developed in 1991 by Linus Torvalds, who wanted to create a free alternative to the proprietary Unix operating systems that were commonly used at the time.


    The FSF and GNU project have contributed significantly to the development of Linux. The GNU project developed many of the tools and utilities that are used in the Linux operating system, including the GCC (GNU Compiler Collection), which is used to compile the Linux kernel and other programs, and the GNU C Library (glibc), which is used to provide standard library functions for the Linux operating system.

    In addition to the FSF and GNU project, there are many other individuals and organizations that have contributed to the development of Linux. Linux is developed by a global community of volunteers who collaborate through online forums and mailing lists, and the operating system has benefited from the contributions of thousands of developers from around the world. Major companies such as IBM, Red Hat, and Intel have also contributed resources and expertise to the development of Linux.
