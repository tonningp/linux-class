# Introduction and Getting Started with Linux

## Topic Objectives

When completed you should be able to answer the following questions:

### Introduction to Linux

* What is an operating system?
* What is free and open source software?
* What does free software mean?
* Why do we need an operating system?
* What operating systems are available to the average computer user?
* Since Linux is so popular as an operating system, why do we choose it?
* What is a kernel?
* What is a kernel programming interface?
* What some of the standards for the Linux kernel and GNU utilities?
* What is the shell?
* What is interprocess communication?
* What is system administration?
* Why is Linux popular with software developers.
[Go To Introduction](./introduction/index.md)

### Getting Started with Linux

* How do you "Log In" to a Linux system?
* What are the advantages of a command line (textual) interface?
* How do you correct mistakes on the command line?
* How can you "kill" a running program?
* How can you edit and repeat previous commands?
* What is the "root" user and why should you be very careful if you are working as one?
* How can you find out how to do more things on the command line, using the command line?
* How are passwords managed in Linux?

[Getting Started With Linux](./getting-started-with-linux/index.md)
